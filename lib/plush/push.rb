require 'singleton'
module Plush
  class Push
    include Singleton

    def initialize
      @sns = Aws::SNS::Client.new
    end

    def sns
      @sns
    end
  end
end