require 'kaminari'

module Plush
  class Engine < ::Rails::Engine
    isolate_namespace Plush

  end
  def self.table_name_prefix
    'plush_'
  end
end
