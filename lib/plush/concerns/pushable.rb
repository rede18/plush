module Plush
  module Concerns
    module Pushable
      extend ActiveSupport::Concern

      def publish(message)
        channel.publish(title, message)
      end

      def subscribe_an_installation(installation)
        Plush::subscribe_an_installation(installation, channel_name)
      end

      def unsubscribe_an_installation(installation)
        Plush::unsubscribe_an_installation(installation, channel_name)
      end

      def channel
        Channel.find_or_create_by(name: channel_name)
      end

      def notifications
        Notification.joins(:channel).where(plush_channels: {name: channel_name})
      end

      def channel_name
        return "#{self.model_name.name}_#{self.id}" if Rails.env.production?
        "#{self.model_name.name}_#{self.id}_dev"
      end

    end
  end
end
