module Plush
  module Concerns
    module User
      extend ActiveSupport::Concern

      def subscribe_to_my_channels(installation)
        my_channels.each { |c| Plush::subscribe_an_installation(installation, c) } if try(:my_channels)
      end

    end
  end
end
