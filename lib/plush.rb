require 'plush/concerns/user'
require 'plush/concerns/pushable'
require 'plush/concerns/subscription'
require 'plush/push'
require "plush/engine"

require 'aws-sdk'

module Plush
  # Your code goes here...

  mattr_accessor :apns_sandbox
  mattr_accessor :apns
  mattr_accessor :gcm
  mattr_accessor :user_class
  mattr_accessor :push_categories

  def self.user_class
    @@user_class.constantize
  end

  def self.subscribe_an_installation(installation, channel_name)
    channel = first_or_create_channel(channel_name)

    subscribe_installation_on_sns(installation, channel)
  end

  def self.unsubscribe_an_installation(installation, channel_name)
    channel = first_or_create_channel(channel_name)

    subscription = Plush::Subscription.find_by(channel: channel, installation: installation)

    unsubscribe_installation_on_sns(subscription)
  end


  def self.first_or_create_channel(name)
    Channel.find_or_create_by(name: name)
  end

  private

    def self.unsubscribe_installation_on_sns(subscription)
      if subscription
        result = Plush::Push.instance.sns.unsubscribe(subscription_arn: subscription.subscription_arn)

        if result.error.nil?
          subscription.delete
          true
        else
          false
        end
      else
        false
      end
    end

    def self.subscribe_installation_on_sns(installation, channel)
      unless installation.channels.include?(channel)
        result = Plush::Push.instance.sns.subscribe(topic_arn: channel.topic_arn, protocol: :application, endpoint: installation.endpoint_arn)
        if result.error.nil?
          installation.subscriptions.create(subscription_arn: result.subscription_arn, channel: channel)
        else
          return false
        end
      end
    end

end
