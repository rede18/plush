Plush.user_class = "User"
Plush.push_categories = [:general]

Plush.apns = 'arn:aws:sns:us-east-1:571442537128:app/APNS/festify_apns_production'
Plush.apns_sandbox = 'arn:aws:sns:us-east-1:571442537128:app/APNS_SANDBOX/festify_ios_sandbox'
Plush.gcm = 'arn:aws:sns:us-east-1:571442537128:app/GCM/Festify-Android'
