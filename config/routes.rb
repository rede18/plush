Plush::Engine.routes.draw do
  post 'register', to: 'push_notifications#register'
  post 'unregister', to: 'push_notifications#unregister'
  post 'subscribe', to: 'push_notifications#subscribe'
  post 'unsubscribe', to: 'push_notifications#unsubscribe'
  post 'sync', to: 'push_notifications#sync'
  resources :installations, param: :device_token, only: [:show, :update]
  resources :notifications, only: [:index]
end
