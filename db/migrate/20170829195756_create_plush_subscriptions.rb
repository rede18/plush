class CreatePlushSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :plush_subscriptions do |t|
      t.references :plush_channel, foreign_key: true
      t.references :plush_installation, foreign_key: true
      t.string :subscription_arn

      t.timestamps
    end
  end
end
