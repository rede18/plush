class CreatePlushChannels < ActiveRecord::Migration[5.1]
  def change
    create_table :plush_channels do |t|
      t.string :name
      t.string :topic_arn

      t.timestamps
    end
  end
end
