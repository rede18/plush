class AddCategoryToPlushNotification < ActiveRecord::Migration[5.1]
  def change
    add_column :plush_notifications, :category, :integer
  end
end
