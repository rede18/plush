class CreatePlushInstallations < ActiveRecord::Migration[5.1]
  def change
    create_table :plush_installations do |t|
      t.string :device_token
      t.string :endpoint_arn
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
