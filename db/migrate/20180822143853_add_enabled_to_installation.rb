class AddEnabledToInstallation < ActiveRecord::Migration[5.2]
  def change
    add_column :plush_installations, :enabled, :boolean, default: true
  end
end
