class AddDataToPlushNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :plush_notifications, :data, :json, default: {}
  end
end
