class CreatePlushNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :plush_notifications do |t|
      t.string :title
      t.string :description
      t.references :plush_channel, foreign_key: true
      t.string :message_arn
      t.datetime :when

      t.timestamps
    end
  end
end
