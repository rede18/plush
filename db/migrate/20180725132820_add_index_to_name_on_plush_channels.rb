class AddIndexToNameOnPlushChannels < ActiveRecord::Migration[5.2]
  def change
    add_index :plush_channels, :name, unique: true
  end
end
