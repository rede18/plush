require 'rails_helper'

RSpec.describe Plush::Channel, type: :model do


  it 'has many notifications' do
    should have_many(:notifications).class_name('Plush::Notification')
  end

  it 'has many installations through subscription' do
    #should have_many(:installations).class_name('Plush::Installation').through(:subscriptions).class_name('Plush::Subscription')
  end

  it 'has many subscriptions' do
    should have_many(:subscriptions).class_name('Plush::Subscription')
  end




  it 'publishes(create notification) to channel' do

    # creates user channel
    FactoryBot.create(:user)
    FactoryBot.create(:channel)
    # validates notification creation
    channel = Plush::Channel.last
    expect{channel.publish('Olha o Churros', 'Tem de chocolate e leite condensado.')}.to change{Plush::Notification.count}.by 1
    # match notification title
    notification = channel.notifications.last
    expect(notification.title).to be_eql 'Olha o Churros'
  end
end
