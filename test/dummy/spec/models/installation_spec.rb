require 'rails_helper'

RSpec.describe Plush::Installation, type: :model do

  it 'has many subscriptions' do
    should have_many(:subscriptions).class_name('Plush::Subscription')
  end

end
