require 'rails_helper'

RSpec.describe User, type: :model do


  before(:all) do
    User.destroy_all
    Plush::Channel.destroy_all
  end

  context 'CRUD operations' do

    it 'creates user and user channel' do
      user = FactoryBot.create(:user)
      # creates user channel before_save
      expect{user.channel}.to change{Plush::Channel.count}.by(1)
      # creates user
      expect(user.created_at?).to be_truthy
    end

  end


end
