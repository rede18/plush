module Plush
  module RequestSpecHelper
    include Warden::Test::Helpers

    def self.included(base)
      base.before(:each) {Warden.test_mode!}
      base.after(:each) {Warden.test_reset!}
    end

    def sign_in(user, password = '12345678')
    'Bearer ' + user.encode_my_jwt
  end
    def sign_out(resource)
      logout(warden_scope(resource))
    end

    private

    def warden_scope(resource)
      resource.class.name.underscore.to_sym
    end

  end
end
