require 'rails_helper'

RSpec.describe "Installations", type: :request do
  before(:all) do
    User.destroy_all
    random_string = rand(36**6).to_s(36)
    @user = User.create(email: "email_#{random_string}@email.com", password: '12345678', password_confirmation: '12345678')
    @user_token = sign_in @user

    random_string = rand(36**5).to_s(36)
    @user_2 = User.create(email: "email2_#{random_string}@email.com", password: '12345678', password_confirmation: '12345678')
    @user_2_token = sign_in @user_2
    @token_1 = rand(36**6).to_s(16).upcase
    @token_2 = rand(36**6).to_s(16).upcase

    post '/plush/register.json', params: {
      device: {
        device_token: @token_1,
        platform: "ios",
        sandbox: true
      }
    }

  end

  describe "PUT /installations.json" do
    it "updates installation to false" do
      put "/plush/installations/#{@token_1}.json", params: {
          installation: {
              enabled: false
          }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['enabled']).to eq(false)

    end

    it "updates installation to true" do
      put "/plush/installations/#{@token_1}.json", params: {
          installation: {
              enabled: true
          }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['enabled']).to eq(true)

    end
  end

  describe "GET /installations/?.json" do
    it "get installation details" do
      get "/plush/installations/#{@token_1}.json"

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['enabled']).to eq(true)

    end
  end

end
