require 'rails_helper'

RSpec.describe "Notifications", type: :request do
  before(:all) do
    User.destroy_all
    random_string = rand(36**6).to_s(36)
    @user = User.create(email: "email_#{random_string}@email.com", password: '12345678', password_confirmation: '12345678')
    @user_token = sign_in @user

    random_string = rand(36**5).to_s(36)
    @user_2 = User.create(email: "email2_#{random_string}@email.com", password: '12345678', password_confirmation: '12345678')
    @user_2_token = sign_in @user_2
    @token_1 = rand(36**6).to_s(16).upcase
    @token_2 = rand(36**6).to_s(16).upcase

    post '/plush/register.json', headers: {
      Authorization: @user_2_token
    }, params: {
      device: {
        device_token: @token_1,
        platform: "ios",
        sandbox: true
      }
    }

    post '/plush/register.json', headers: {
      Authorization: @user_2_token
    }, params: {
      device: {
        device_token: @token_2,
        platform: "ios",
        sandbox: true
      }
    }

    post '/plush/subscribe.json', headers: {
      Authorization: @user_2_token
    }, params: {
      device: {
        name: 'CanalY',
        device_token: @token_2
      }
    }

    @user_2.channel.publish('Teste 1', "Descrição do teste 1")
    @user_2.channel.publish('Teste 2', "Descrição do teste 2")
  end

  describe "GET /notifications.json" do
    it "returns all user notifications if is logged" do
      get '/plush/notifications.json', headers: {
        Authorization: @user_2_token
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json).to have_exactly(2).items
    end

    it "doesn't return notifications if is not logged" do

    end
  end

  describe "POST /register" do
    it "registers a device if is logged" do
      post '/plush/register.json', headers: {
        Authorization: @user_2_token
      }, params: {
        device: {
          device_token: rand(36**6).to_s(16).upcase,
          platform: "ios",
          sandbox: true
        }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'The device was registered successfully.'
    end

    it "register a device if is not logged" do
      post '/plush/register.json', params: {
        device: {
          device_token: rand(36**6).to_s(16).upcase,
          platform: "ios",
          sandbox: true
        }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'The device was registered successfully.'
    end
  end

  describe "POST /subscribe" do
    it 'subscribes user devices to a channel if is logged' do
      post '/plush/subscribe.json', headers: {
        Authorization: @user_2_token
      }, params: {
        device: {
          name: 'CanalX',
          device_token: @token_2
        }
      }

      expect(response).to have_http_status(:ok)

      json = JSON.parse response.body
      expect(json['success']).to eq 'Successfully subscribed from channel'
    end

    it 'subscribes user devices to its channels if is logged' do
      post '/plush/sync.json', headers: {
          Authorization: @user_2_token
      }, params: {
          device: {
              device_token: @token_2
          }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'User subscribed its channels.'
    end

    it 'subscribe device to a channel if is not logged' do
      post '/plush/subscribe.json', params: {
        device: {
          name: 'General',
          device_token: @token_2
        }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'Successfully subscribed from channel'
    end

    it 'doesn\'t subscribe user devices to a channel if is not logged and no device_token was sent' do
      post '/plush/subscribe.json', params: {
          device: {
              name: 'General'
          }
      }

      expect(response).to have_http_status(:unprocessable_entity)
      json = JSON.parse response.body
      expect(json['error']).to eq 'You must send your device token.'
    end
  end



  describe "POST /unsubscribe" do
    it 'unsubscribes user devices to a channel if is logged' do
      post '/plush/unsubscribe.json', headers: {
        Authorization: @user_2_token
      }, params: {
        device: {
          name: 'CanalY',
          device_token: @token_2
        }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'Successfully unsubscribed from channel'
    end

    it 'doesn\'t unsubscribe user devices to a channel if is not logged and has no device token' do
      post '/plush/unsubscribe.json', params: {
        device: {
          name: 'General'
        }
      }

      expect(response).to have_http_status(:unprocessable_entity)
      json = JSON.parse response.body
      expect(json['error']).to eq 'You must send your device token.'
    end

    it 'unsubscribes user devices to a channel if is not logged and has device token' do
      post '/plush/subscribe.json', params: {
          device: {
              name: 'General',
              device_token: @token_2
          }
      }

      post '/plush/unsubscribe.json', params: {
          device: {
              name: 'General',
              device_token: @token_2
          }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'Successfully unsubscribed from channel'
    end
  end

  describe "POST /unregister" do
    it 'unregisters a device if is logged' do
      post '/plush/unregister', headers: {
        Authorization: @user_2_token
      }, params: {
        device: {
          device_token: @token_1
        }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'Successfully unsubscribed from all channels'
    end

    it 'unregister a device if is not logged' do
      post '/plush/unregister.json', params: {
        device: {
          device_token: @token_2
        }
      }

      expect(response).to have_http_status(:ok)
      json = JSON.parse response.body
      expect(json['success']).to eq 'Successfully unsubscribed from all channels'
    end
  end
end
