FactoryBot.define do
  factory :notification, class: 'Plush::Notification' do
    association :channel
    title 'Lorem ipsummmmmmm'
    description 'olha o churrossssss tem churros? tem'
    message_arn 'something random'
    created_at { Time.now }
  end
end