FactoryBot.define do
  factory :subscription, class: 'Plush::Subscription' do
    association :channel
    association :installation
    subscription_arn 'subscription_arn'

  end
end



