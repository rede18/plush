FactoryBot.define do
  factory :installation, class: 'Plush::Installation' do
    device_token 'bca7874ca6b21a78c679c8c2722fd84a2582f9ead5590ae9e795a56a5651db4e'
    platform_application_arn Plush.apns_sandbox
    #endpoint_arn  'arn:aws:sns:us-east-1:305295129279:endpoint/GCM/instashirt/df7d9f42-14c7-34ec-9fb7-a212f755229e'
  end
end
