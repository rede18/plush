FactoryBot.define do
  factory :user do
    name 'John Snow'
    sequence(:email){|n| "JoaoNeves_#{n}@gmail.com"}
    role 'manager'
    password 'segredo123'
    cpf '11100011100'
  end
end
