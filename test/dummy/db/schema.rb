# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_22_143853) do

  create_table "plush_channels", force: :cascade do |t|
    t.string "name"
    t.string "topic_arn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_plush_channels_on_name", unique: true
  end

  create_table "plush_installations", force: :cascade do |t|
    t.string "device_token"
    t.string "endpoint_arn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "enabled", default: true
  end

  create_table "plush_notifications", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.integer "plush_channel_id"
    t.string "message_arn"
    t.datetime "when"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category"
    t.json "data", default: {}
    t.index ["plush_channel_id"], name: "index_plush_notifications_on_plush_channel_id"
  end

  create_table "plush_subscriptions", force: :cascade do |t|
    t.integer "plush_channel_id"
    t.integer "plush_installation_id"
    t.string "subscription_arn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plush_channel_id"], name: "index_plush_subscriptions_on_plush_channel_id"
    t.index ["plush_installation_id"], name: "index_plush_subscriptions_on_plush_installation_id"
  end

  create_table "refresh_tokens", force: :cascade do |t|
    t.string "token", default: "", null: false
    t.integer "user_id"
    t.datetime "expires_at"
    t.string "device_name", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_refresh_tokens_on_token", unique: true
    t.index ["user_id"], name: "index_refresh_tokens_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "cpf"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
