Rails.application.routes.draw do
  devise_for :users
  get 'home/index'
  mount JwtDevise::Engine, at: '/jwt'
  resources :users
  mount Plush::Engine => "/plush"
  root to: "home#index"
end
