class User < ApplicationRecord

  enum role:[:admin, :manager, :user]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :jwt_authenticatable

  include Plush::Concerns::Subscription
  include Plush::Concerns::Pushable
  include Plush::Concerns::User

  def my_channels
    [channel_name, 'male']
  end

end
