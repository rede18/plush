require 'test_helper'

module Plush
  class PushNotificationsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get register" do
      get push_notifications_register_url
      assert_response :success
    end

    test "should get unregister" do
      get push_notifications_unregister_url
      assert_response :success
    end

    test "should get subscribe" do
      get push_notifications_subscribe_url
      assert_response :success
    end

    test "should get unsubscribe" do
      get push_notifications_unsubscribe_url
      assert_response :success
    end

  end
end
