module Plush
  class Subscription < ApplicationRecord
    belongs_to :channel, class_name:'Plush::Channel', foreign_key: 'plush_channel_id' , inverse_of: :subscriptions
    belongs_to :installation, class_name:'Plush::Installation', foreign_key: 'plush_installation_id' , inverse_of: :subscriptions

    before_save :register_subscription_on_sns

    private

    def register_subscription_on_sns
      if self.subscription_arn.blank?
        result = Push.instance.sns.subscribe(topic_arn: self.channel.topic_arn, protocol: :application, endpoint: self.installation.endpoint_arn)
        if result.error.nil?
          self.subscription_arn = result.subscription_arn
        end
      end
    end

  end

end
