module Plush
  class Channel < ApplicationRecord
    has_many :notifications, class_name:'Plush::Notification', foreign_key: 'plush_channel_id', inverse_of: :channel, dependent: :destroy
    has_many :subscriptions, class_name:'Plush::Subscription', foreign_key: 'plush_channel_id', inverse_of: :channel, dependent: :destroy
    has_many :installations, class_name:'Plush::Installation', through: :subscriptions

    before_save :register_topic_on_sns

    def publish(title, message)
      self.notifications.create(title: title, description: message)
    end

    private

    def register_topic_on_sns
      if self.topic_arn.blank?
        result = Push.instance.sns.create_topic(name: self.name)
        if result.error.nil?
          self.topic_arn = result.topic_arn
        end
      end
    end

  end

end
