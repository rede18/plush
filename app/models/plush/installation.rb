module Plush
  class Installation < ApplicationRecord
    #has_paper_trail
    after_validation :create_on_sns
    after_validation :enable_or_disable_on_sns, on: [ :update ]

    attr_accessor :platform_application_arn

    has_many :subscriptions, class_name:'Plush::Subscription', foreign_key: 'plush_installation_id', inverse_of: :installation, dependent: :destroy
    has_many :channels, through: :subscriptions

    def get_name
      self.device_token
    end

    def subscribe_to_default_channels(platform)
      # Subscribe to general channel
      Plush::subscribe_an_installation(self, general_channel_name)

      # Subscribe to platform
      Plush::subscribe_an_installation(self, platform_channel_name(platform))
    end

    def unsubscribe_all
      self.subscriptions.each do |subscription|
        unless [general_channel_name, platform_channel_name('ios'), platform_channel_name('android')].include? subscription.channel.name
          result = Plush::Push.instance.sns.unsubscribe(subscription_arn: subscription.subscription_arn)
          if result.error.nil?
            subscription.delete
          end
        end
      end
    end

    private

    def create_on_sns
      if self.endpoint_arn.nil?
        result = Push.instance.sns.create_platform_endpoint(platform_application_arn: platform_application_arn, token: device_token)
        if result.error.nil?
          self.endpoint_arn = result.endpoint_arn
        else
          self.errors.add(:endpoint_arn, result.error)
        end
      end
    end

    def enable_or_disable_on_sns
      result = Push.instance.sns.set_endpoint_attributes(endpoint_arn: self.endpoint_arn,
                                                attributes: {Enabled: self.enabled.to_s}
      )
      unless result.error.nil?
        self.errors.add(:enabled, result.error)
      end
    end

    def general_channel_name
      return 'General' if Rails.env.production?
      'General_dev'
    end

    def platform_channel_name(platform)
      return platform if Rails.env.production?
      "#{platform}_dev"
    end
  end

end
