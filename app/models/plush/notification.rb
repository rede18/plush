module Plush
  class Notification < ApplicationRecord
    belongs_to :channel, class_name:'Plush::Channel', foreign_key: 'plush_channel_id' , inverse_of: :notifications

    validates :title, length: {maximum: 30}
    validates :description, length: {maximum: 255}
    validates :channel, presence: true
    validates :title, presence: true
    validates :description, presence: true

    enum category: Plush.push_categories

    before_create :check_if_needs_publish

    def channel=(channel)
      write_attribute :plush_channel_id, Plush::Channel.find_or_create_by(name: channel.name).id
    end

    def publish_now
      result = publish_on_sns
      throw :abort unless result.error.nil?
      self.message_arn = result.message_id
    end

    private

    def check_if_needs_publish
      publish_now if self.when.nil?
    end

    # https://developer.apple.com/documentation/usernotifications/declaring_your_actionable_notification_types
    # https://firebase.google.com/docs/cloud-messaging/concept-options
    def publish_on_sns
      msg = {
          :default => "#{self.title} #{self.description}",
          :APNS_SANDBOX => {
              aps: {
                  alert: {
                      title: self.title,
                      body: self.description
                  },
                  badge: 1,
                  sound: 'default'
              },
              custom_data: self.data
          }.to_json,
          :APNS => {
              aps: {
                  alert: {
                      title: self.title,
                      body: self.description
                  },
                  badge: 1,
                  sound: 'default'
              },
              custom_data: self.data
          }.to_json,
          :GCM => {
              data: {
                custom_notification: {
                    title: self.title,
                    body: self.description,
                    custom_data: self.data,
                    sound: 'default',
                    priority: "high",
                    show_in_foreground: true,
                    icon: 'ic_notification',
                    channel: 'default'
                }
              }
          }.to_json
      }
      Push.instance.sns.publish(topic_arn: self.channel.topic_arn, message: msg.to_json, message_structure: :json)
    end
  end
end

# {
#     "GCM": "{ \"data\": { \"title\": \"oi\",  \"text\": \"textinho\",  \"show_in_foreground\": true } }"
# }
