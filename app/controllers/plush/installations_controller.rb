require_dependency "plush/application_controller"

module Plush
  class InstallationsController < ApplicationController
    before_action :set_installation, only: [:show, :update]

    # GET /installations/1
    # GET /installations/1.json
    def show
    end

    # PATCH/PUT /installations/1
    # PATCH/PUT /installations/1.json
    def update
      respond_to do |format|
        if @installation.update(installation_params)
          format.json {render :show, status: :ok, location: @installation}
        else
          format.json {render json: @installation.errors, status: :unprocessable_entity}
        end
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_installation
      @installation = Installation.find_by!(device_token: params[:device_token])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def installation_params
      params.require(:installation).permit(:endpoint_arn, :user_id, :enabled)
    end
  end
end
