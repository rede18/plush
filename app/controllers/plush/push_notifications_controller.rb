module Plush
  class PushNotificationsController < ApplicationController
    # before_action :authenticate_user
    before_action :set_sns

    def register
      # Initializes variables
      result = false

      # Retrieve the platform endpoint
      platform_endpoint = installation
      # If this is a first-time registration
      if platform_endpoint and platform_endpoint.endpoint_arn.present?
        # Retrieve attributes from SNS, if it throws an exception the endpoint was deleted
        begin
          response = @sns.get_endpoint_attributes(endpoint_arn: platform_endpoint.endpoint_arn)
          # Checks if the device token is the same or it became disabled
          if need_updates?(response)
            @sns.set_endpoint_attributes(endpoint_arn: platform_endpoint.endpoint_arn,
                                         attributes: {Token: device_params[:device_token], Enabled: 'true'}
            )
          end
          result = true
        rescue Aws::SNS::Errors::NotFoundException
          result = create_endpoint
        end
      else
        result = create_endpoint
      end
      if result
        create_default_subscriptions(installation)

        render status: :ok, json: {success: 'The device was registered successfully.'}
      else
        render status: :unprocessable_entity, json: {error: result.error}
      end

    end

    def unregister
      # Should check if user is logged in and installation is subscribing its channel?
      if installation.nil?
        render json: {error: "You must send your device token."}, status: :unprocessable_entity
      elsif installation.unsubscribe_all
        render json: {success: "Successfully unsubscribed from all channels"}, status: :ok
      else
        render json: {error: "Could not unsubscribe all user channels from Amazon SNS"}, status: :internal_server_error
      end

    end

    def subscribe
      if installation.nil?
        render json: {error: "You must send your device token."}, status: :unprocessable_entity
      elsif Plush::subscribe_an_installation(installation, device_params[:name])
        render json: {success: "Successfully subscribed from channel"}, status: :ok
      else
        render json: {error: "Could not subscribe from Amazon SNS"}, status: :internal_server_error
      end
    end

    def sync
      if installation.nil?
        render json: {error: "You must send your device token."}, status: :unprocessable_entity
      else
        if current_user.try(:subscribe_to_my_channels, installation)
          render json: {success: "User subscribed its channels."}, status: :ok
        else
          render json: {error: "User could not subscribe its channels."}, status: :unprocessable_entity
        end
      end
    end

    def unsubscribe
      if installation.nil?
        render json: {error: "You must send your device token."}, status: :unprocessable_entity
      elsif Plush::unsubscribe_an_installation(installation, device_params[:name])
        render json: {success: "Successfully unsubscribed from channel"}, status: :ok
      else
        render json: {error: "Could not unsubscribe from Amazon SNS or Subscription is nil"}, status: :unprocessable_entity
      end
    end

    private
    def set_sns
      @sns = Push.instance.sns
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def device_params
      params.require(:device).permit(:device_token, :platform, :sandbox, :name)
    end

    def create_endpoint
      result = @sns.create_platform_endpoint(platform_application_arn: platform_application_arn, token: device_params[:device_token])
      if result.error.nil?
        @installation = Installation.find_or_create_by(device_token: device_params[:device_token], endpoint_arn: result.endpoint_arn)
        @installation.subscribe_to_default_channels(device_params[:platform])
        true
      else
        false
      end
    end

    def create_default_subscriptions(installation)
      installation.subscribe_to_default_channels(device_params[:platform])
    end

    def need_updates?(response)
      response.attributes['Token'] != device_params[:device_token] or response.attributes['Enabled'] == 'false'
    end

    def platform_application_arn
      if device_params[:platform] == 'ios'
        if device_params[:sandbox]
          Plush.apns_sandbox
        else
          Plush.apns
        end
      elsif device_params[:platform] == 'android'
        Plush.gcm
      else
        ''
      end
    end

    def installation
      Installation.find_by(device_token: device_params[:device_token])
    end

  end
end
