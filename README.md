# Plush
Awesome gem made for aws notification push service on back-end.
Covers GCM and iOS.

## Dependencies

    gem 'devise'
    gem 'aws-sdk'


## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'plush'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install plush
```

```bash
$ bin/rails plush:install:migrations
```

## Configuration
Add concerns to you user model class:
```ruby
  include Plush::Concerns::PushNotification
  include Plush::Concerns::ChannelUser
```
with PushNotification, the user gets aws push notification services,
and the following association will be added
```ruby
has_many :installations, class_name: 'Plush::Installation', foreign_key: 'user_id', inverse_of: :user
```

with ChannelUser, the user gets aws channel services,
and the following callback will be added:

```ruby
before_save :create_my_channel
```

Mount engine to your project's config/routes.rb:

```ruby
mount Plush::Engine, at: '/plush'
```

##Routes:
register device:
```ruby
  post 'push_notifications/register'
```
platform can be either 'ios' or 'android'. For platform 'ios' you can use sandbox true or false.
```json
device:{
  device_token: "ka2o3c96lrkgi...",
  platform: "ios",
  sandbox: true
}
```


Unregister device:
```ruby
  post 'push_notifications/unregister'
```

```json
device:{
  device_token: "ka2o3c96lrkgi..."
}
```


```ruby
  post 'push_notifications/subscribe'
```
Use this endpoint to subscribe to a channel
```json
channel:{
  name: "General"
}
```

```ruby
  post 'push_notifications/unsubscribe'
```
Use this endpoint to unsubscribe to a channel
```json
channel:{
  name: "General"
}
```

## Initializers

Notification model have a category attribute that can be overridden.
ex:
```ruby        
Plush.push_categories = [ :promotions, :informs, :cupons ]
```

Add Your gcm and apns configurations from AWS:
ex:
```ruby  
# apns
Plush.apns = 'arn:aws:sns:us-east-1:012006636466:app/APNS/BRSeguros-iOS-Production'

# optional
Plush.apns_sandbox = 'arn:aws:sns:us-east-1:310506636466:app/APNS_SANDBOX/BRSeguros-iOS-Dev'      

# gcm
Plush.gcm = 'arn:aws:sns:us-east-1:10020095130264:app/GCM/instashirt'
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
