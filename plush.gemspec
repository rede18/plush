$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "plush/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "plush"
  s.version     = Plush::VERSION
  s.authors     = ["marcelo"]
  s.email       = ["marcelobbff@gmail.com"]
  s.homepage    = "https://www.yimobile.com.br"
  s.summary     = "Summary of Plush."
  s.description = "Description of Plush."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.1.4"
  s.add_dependency 'devise'
  s.add_dependency 'paper_trail'
  s.add_dependency 'kaminari'

  s.add_development_dependency 'byebug'
  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'rails_admin'
  s.add_development_dependency 'rspec-rails', '~> 3.6.0'
  s.add_development_dependency 'rspec-collection_matchers'
  s.add_development_dependency 'shoulda-matchers', '~> 3.1'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'figaro'
end
